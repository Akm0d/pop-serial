Horizontal merge
----------------

To add ``pop-serial`` to your project, follow these steps:

Add "pop-serial" to your project's requirements file.

requirements/base.txt

.. code-block::

    pop
    pop-serial

In your main ``init`` file, extend pop with the "serial" dynamic namespace

my_project/src/init.py

.. code-block:: python

    def __init__(hub):
        hub.pop.sub.add(dyne_name="serial")

Now you can call "serial" plugins via the hub.

.. code-block:: python

    def my_func(hub, serial_plugin: str = "msgpack"):
        data = {}
        serialized_data: bytes = hub.serial[serial_plugin].dump(data)
        deserialized_data = hub.serial[serial_plugin].load(serialized_data)

Vertical merge
--------------

In order to extend this project, add the "serial" dyne to your project's `conf.py`.

my_project/conf.py

.. code-block:: python

    DYNE = {"serial": ["serial"]}

Now add your own serial pluigns to ``my_project_root/my_project/serial/``

Your plugin must implement two functions:

.. code-block:: python

    def dump(hub, data) -> bytes:
        return b"serialized data as bytecode"


    def load(hub, data: bytes):
        return "deserialized data from bytecode"
